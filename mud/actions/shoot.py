from .action import Action2, Action3
from mud.events import ShootEvent, ShootWithEvent

class ShootAction(Action2):
    EVENT = ShootEvent
    ACTION = "shoot"
    RESOLVE_OBJECT = "resolve_for_use"

class ShootWithAction(Action3):
    EVENT = ShootWithEvent
    ACTION = "shoot-with"
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
